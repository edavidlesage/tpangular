import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
title !:String;
date !:Date;
nombre !:number;
nombre2 !:number;
total !:number;

  constructor() { }

  ngOnInit(): void {
    this.title = "Essai Angular";
    this.date = new Date();
    this.nombre = 2;
    this.nombre2 = 23;
    this.total=this.onCalculer(this.nombre,this.nombre2);
     console.log(this.total);
    // alert(this.total);
  }

  onCalculer(nombre:number,nombre2:number) : number {
  return  this.nombre + this.nombre2;
  }

  retourne!: string;
  onKeyUp(x:any){
    this.retourne += x.target.value+ '/';
  }

}
