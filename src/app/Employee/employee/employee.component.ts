import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../employee.service";
import {NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Employee} from "../employee";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  public employees!: Employee[];
  public employee!:Employee;
  mode:string = "list";

  constructor(private employeeService:EmployeeService) { }

  ngOnInit() {
    this.getEmployees();
  }

  public getEmployees(): void {
    this.employeeService.getEmployees().subscribe(
      (response: Employee[]) => {
        this.employees = response;
        console.log(this.employees);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddEmloyee(addForm: NgForm): void {
    console.log(addForm);
    this.employeeService.addEmployee(addForm.value).subscribe(
      (response: Employee) => {
        console.log(response);
        this.getEmployees();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  onList() {
    this.mode="list";
  }

  onAddForm() {
    this.mode="form";
  }

  public onDeleteEmloyee(employeeId: number): void {
    let conf = confirm("Etes vous sur de vouloir supprimer ?");
    if (!conf) return;
    this.employeeService.deleteEmployee(employeeId).subscribe(
      (response: void) => {
        console.log(response);
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onSearchEmpById(employeeId: number): void {
    this.employeeService.getOneEmployee(employeeId)
      .subscribe(data => {
        this.employee = data;
        this.mode="formModif"
        console.log(this.employee);
      }, err => {
        console.log(err);
      })
  }
}
