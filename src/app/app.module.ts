import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import * as fr from '@angular/common/locales/fr';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test23Component } from './test23/test23.component';
import { Test2Component } from './test2/test2.component';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { EmployeeComponent } from './Employee/employee/employee.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test23Component,
    Test2Component,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, HttpClientModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default);
  }
}
