import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test23',
  templateUrl: './test23.component.html',
  styleUrls: ['./test23.component.css']
})
export class Test23Component implements OnInit {
nombre1!:number;
nombre2!:number;
operateur:any;
total!:number;

imageUrl!: string;
modeTotal : any;

  constructor() { }

  ngOnInit(): void {
    this.modeTotal = 0;
    this.imageUrl = "https://img-19.commentcamarche.net/cI8qqj-finfDcmx6jMK6Vr-krEw=/1500x/smart/b829396acc244fd484c5ddcdcb2b08f3/ccmcms-commentcamarche/20494859.jpg";
  }

  onCalculer({data}: { data: any }) {
    console.log(data);
    this.nombre1=data.nombre1;
    this.nombre2 = data.nombre2;
    this.operateur = data.operateur;

    if(this.operateur=='+'){
      this.total = Number(this.nombre1) + Number(this.nombre2);
      this.modeTotal=1;
    }else if(this.operateur=='-'){
      this.total = Number(this.nombre1) - Number(this.nombre2);
    }
    console.log(this.total);
  }

}
